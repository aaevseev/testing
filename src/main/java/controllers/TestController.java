package controllers;

import base.Question;
import com.google.gson.Gson;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class TestController {

    private static final int MAX_CHOICE = 4; // Максимальное количество вариантов ответа

    @FXML
    Label textQuestion;
    @FXML
    RadioButton firstChoice;
    @FXML
    RadioButton secondChoice;
    @FXML
    RadioButton thirdChoice;
    @FXML
    RadioButton fourthChoice;

    private ToggleGroup toggleGroup;

    private int count = 0; // Счётчик пройденных вопросов
    private int correctAnswerCount = 0; // Счётчик правильных ответов

    private Question[] questions;
    private ArrayList<String> correctAnswers = new ArrayList<>(); // Список правильных ответов на вопросы
    private ArrayList<RadioButton> radioButtonsList = new ArrayList<>();

    @FXML
    public void initialize() {

        radioButtonsList = new ArrayList<>();

        radioButtonsList.add(firstChoice);
        radioButtonsList.add(secondChoice);
        radioButtonsList.add(thirdChoice);
        radioButtonsList.add(fourthChoice);

        toggleGroup = new ToggleGroup();

        for (RadioButton aRadioButtonsList : radioButtonsList) {
            aRadioButtonsList.setToggleGroup(toggleGroup);
        }

        getObjectFromJson(); // Вытаскиваем все вопросы из json-файлы и переводим объект
        initAnswer(new ActionEvent()); // Отображаем первый вопрос
    }

    private void getObjectFromJson() {
        try {
            InputStream file = this.getClass().getClassLoader().getResourceAsStream(("questions.json"));
            String json = IOUtils.toString(file, "UTF-8");
            questions = new Gson().fromJson(json, Question[].class);

            //Заполняем массив верными вопросами для отображаения в экране "Результат"
            for (Question question : questions) {
                for (int i = 0; i < question.answers.size(); i++) {
                    if (question.answers.get(i).isCorrect) {
                        correctAnswers.add(question.answers.get(i).text);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void onNextQuestion(ActionEvent event) {
        if (questions[count].isMulti) {
            for (RadioButton radioButton : radioButtonsList) {
                if (radioButton.isSelected())
                    if (Boolean.parseBoolean(radioButton.getUserData().toString())) correctAnswerCount++;
            }
        } else {
            if (Boolean.parseBoolean(toggleGroup.getSelectedToggle().getUserData().toString())) correctAnswerCount++;
        }

        count++; // Увеличиваем количество пройденных вопросов
        initAnswer(event);
    }

    private void initAnswer(ActionEvent event) {

        if (questions.length != 0 && count < questions.length) {

            textQuestion.setText(questions[count].text);

            for (int i = 0; i < questions[count].answers.size(); i++) {
                // Если вопрос может содержать в себе несколько вариантов, то удаляем radioButton из toggleGroup для многовариативности
                if (questions[count].isMulti) {
                    textQuestion.setText(textQuestion.getText() + " \n(Выберите несколько вариантов)");
                    for (RadioButton radioButton : radioButtonsList) {
                        radioButton.setToggleGroup(null);
                        radioButton.setSelected(false);
                    }
                } else {
                    for (RadioButton aRadioButtonsList : radioButtonsList) {
                        aRadioButtonsList.setToggleGroup(toggleGroup);
                    }
                }

                // Если в вопросе содержятся только три варианта ответа
                if (questions[count].answers.size() == MAX_CHOICE-1) {

                    Iterator<RadioButton> iterator = radioButtonsList.iterator();

                    while (iterator.hasNext()) {
                        RadioButton radioButton = iterator.next();
                        if (!iterator.hasNext()) {
                            radioButton.setVisible(false);
                        } else {
                            radioButton.setText(questions[count].answers.get(i).text);
                            radioButton.setUserData(questions[count].answers.get(i++).isCorrect);
                        }
                    }
                } else {
                    fourthChoice.setVisible(true);
                    for (RadioButton radioButton : radioButtonsList) {
                        radioButton.setText(questions[count].answers.get(i).text);
                        radioButton.setUserData(questions[count].answers.get(i++).isCorrect);
                    }
                }

                toggleGroup.selectToggle(null); // Снимаем все галки для нового вопроса
            }
        } else {
            // Закрываем прошлое окно
            Stage previouslyStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            previouslyStage.close();

            try {
                //Создаём новое окно
                Stage stage = new Stage();

                FXMLLoader loader = new FXMLLoader(getClass().getResource("/result.fxml"));
                Parent root = loader.load();

                stage.setTitle("Результат");
                stage.setScene(new Scene(root));
                stage.setResizable(false);
                stage.centerOnScreen();
                stage.show();

                ResultController controller = loader.getController();
                controller.setResult(correctAnswerCount, correctAnswers);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
