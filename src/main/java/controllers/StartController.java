package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class StartController {

    @FXML
    public void onClickStart(ActionEvent event) {
        try {

            // Закрываем прошлое окно
            Stage previouslyStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            previouslyStage.close();


            //Создаём новое окно
            Stage stage = new Stage();
            Parent root = FXMLLoader.load(getClass().getResource("/test.fxml"));

            stage.setTitle("Тестирование");
            stage.setScene(new Scene(root));
            stage.setResizable(false);
            stage.centerOnScreen();
            stage.show();

        } catch (IOException io) {
            io.printStackTrace();
        }
    }
}
