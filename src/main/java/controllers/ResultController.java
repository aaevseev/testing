package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

import java.util.ArrayList;

public class ResultController {

    @FXML
    public Label correctCountText;
    @FXML
    public Label correctAnswerText;

    private int count = 0;

    void setResult(int countCorrect, ArrayList<String> correctAnswer) {

        correctCountText.setText(correctCountText.getText() + " " + countCorrect + " верных ответа");
        correctAnswerText.setText("Правильные ответы:");

        for (String aCorrectAnswer : correctAnswer) {
            correctAnswerText.setText(correctAnswerText.getText() + "\n " + ++count + ") [" + aCorrectAnswer + "]");
        }
    }
}
